module Examples
  class CharacterClass < RiceBubble::Serializer
    attributes(
      name: enum(%w[fighter wizard cleric thief]),
      level: integer(min: 1, max: 20)
    )
  end

  class Character < RiceBubble::Serializer
    attribute = integer(min: 3, max: 20)

    attributes(
      name: string,
      classes: array(CharacterClass),
      abilities: object({
        strength: attribute,
        dexterity: attribute,
        constitution: attribute,
        intelligence: attribute,
        wisdom: attribute,
        charisma: attribute
      })
    )
  end
end
