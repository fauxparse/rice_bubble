require 'spec_helper'
require_relative './examples/character'

RSpec.describe RiceBubble::Serializer do
  subject(:serializer) do
    Class.new(described_class) do
      attributes(name: string(min: 3))
    end
  end

  describe '#attributes' do
    context 'when defining attributes as a block' do
      it 'defines an attribute' do
        expect(serializer.attributes).to include(:name)
      end
    end
  end

  describe '#call' do
    subject(:result) { serializer.new(object).call }

    context 'with a valid object' do
      let(:object) { { name: 'Spicy Beef' } }

      it { is_expected.to eq(name: 'Spicy Beef') }
    end

    context 'with an invalid object' do
      let(:object) { { name: 'X' } }

      it 'throws an error' do
        expect { result }.to raise_error(RiceBubble::ValidationError)
      end
    end
  end

  context 'with some attributes' do
    describe '#call' do
      subject(:result) { Examples::Character.new(object).call }

      let(:object) do
        {
          name: 'Spicy Beef',
          classes: [
            {
              name: 'fighter',
              level: 3
            },
            {
              name: 'wizard',
              level: 5
            }
          ],
          abilities: {
            strength: 16,
            dexterity: 14,
            constitution: 18,
            intelligence: 10,
            wisdom: 8,
            charisma: 12
          }
        }
      end

      it 'returns the object' do
        expect(result).to eq(object)
      end

      context 'with an object as an argument' do
        subject(:result) { Examples::Character.new.call(object) }

        it 'returns the object' do
          expect(result).to eq(object)
        end
      end

      context 'with no object' do
        subject(:result) { Examples::Character.new.call }

        it 'throws an error' do
          expect { result }.to raise_error(ArgumentError)
        end
      end
    end
  end

  describe '#fetch' do
    let(:serializer) do
      Class.new(described_class) do
        attributes(
          serializer_method: string,
          object_method: string,
          object_key: string
        )

        def serializer_method
          'serializer_method_value'
        end
      end
    end

    let(:object) do
      { object_key: 'object_key_value' }.tap do |obj|
        class << obj
          def object_method
            'object_method_value'
          end
        end
      end
    end

    it 'fetches a serializer method' do
      expect(serializer.new(object).fetch(:serializer_method))
        .to eq('serializer_method_value')
    end

    it 'fetches an object method' do
      expect(serializer.new(object).fetch(:object_method))
        .to eq('object_method_value')
    end

    it 'fetches an object key' do
      expect(serializer.new(object).fetch(:object_key))
        .to eq('object_key_value')
    end
  end

  describe '.method_missing' do
    it 'recognises known attribute types' do
      expect(described_class.string).to be_a RiceBubble::Attributes::String
    end

    it 'raises an error for unknown methods' do
      expect { described_class.not_a_thing }.to raise_error(NoMethodError)
    end
  end

  describe '.respond_to_missing?' do
    it 'recognises known attribute types' do
      expect(described_class.respond_to_missing?(:integer)).to be true
    end

    it 'returns false for unknown methods' do
      expect(described_class.respond_to_missing?(:not_a_thing)).to be false
    end
  end

  describe '.as' do
    it 'is an alias for .serialized' do
      expect(described_class.as(described_class))
        .to be_a RiceBubble::Attributes::Serialized
    end
  end

  describe '.call' do
    subject(:result) { serializer.call(object) }

    context 'with a valid object' do
      let(:object) { { name: 'Spicy Beef' } }

      it { is_expected.to eq(name: 'Spicy Beef') }
    end
  end
end
