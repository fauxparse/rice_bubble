require 'spec_helper'

RSpec.describe RiceBubble::Attributes::Base do
  subject(:attribute) { described_class.new }

  describe '#valid?' do
    it 'is true by default' do
      expect(attribute.valid?(1)).to be true
    end
  end

  describe '#fetch' do
    subject(:fetched) { attribute.fetch(object, :name) }

    context 'when serializing an object' do
      let(:object) { Struct.new(:name).new('Spicy Beef') }

      it { is_expected.to eq 'Spicy Beef' }
    end

    context 'when serializing a hash' do
      let(:object) { { name: 'Spicy Beef' } }

      it { is_expected.to eq 'Spicy Beef' }
    end

    context 'when given a block' do
      let(:attribute) do
        described_class.new { |object, name| object[name].reverse }
      end

      let(:object) { { name: 'Spicy Beef' } }

      it { is_expected.to eq 'feeB ycipS' }
    end
  end
end
