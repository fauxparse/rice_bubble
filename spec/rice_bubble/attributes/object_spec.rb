require 'spec_helper'

RSpec.describe RiceBubble::Attributes::Object do
  subject(:attr) do
    described_class.new(
      {
        name: RiceBubble::Attributes::String.new,
        age: RiceBubble::Attributes::Integer.new
      }
    )
  end

  describe '#valid?' do
    subject(:valid?) { attr.valid?(value) }

    context 'with a valid object' do
      let(:value) do
        {
          name: 'Spicy Beef',
          age: 16
        }
      end

      it { is_expected.to be true }
    end

    context 'with an invalid object' do
      let(:value) do
        {
          name: 'Spicy Beef',
          age: 'old'
        }
      end

      it { is_expected.to be false }
    end
  end

  describe '#call' do
    subject(:result) { attr.call(value, path: 'person') }

    context 'with a hash' do
      let(:value) do
        {
          name: 'Spicy Beef',
          age: 16
        }
      end

      it { is_expected.to eq(value) }
    end

    context 'with an object' do
      let(:value) do
        Struct.new(:name, :age).new('Spicy Beef', 16)
      end

      it { is_expected.to eq(value.to_h) }
    end

    context 'with an invalid value' do
      let(:value) do
        {
          name: 'Spicy Beef',
          age: 'old'
        }
      end

      it 'throws an error' do
        expect { result }
          .to raise_error(
            RiceBubble::ValidationError,
            'person.age expected an integer but received "old"'
          )
      end
    end
  end
end
