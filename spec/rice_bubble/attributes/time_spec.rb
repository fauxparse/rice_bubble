require 'spec_helper'

RSpec.describe RiceBubble::Attributes::Time do
  subject(:attr) { described_class.new(**options) }

  let(:options) { {} }

  describe '#valid?' do
    subject(:valid?) { attr.valid?(value) }

    context 'with a Time' do
      let(:value) { Time.now }

      it { is_expected.to be true }
    end

    context 'with a DateTime' do
      let(:value) { DateTime.now }

      it { is_expected.to be true }
    end

    context 'with a Date' do
      let(:value) { Date.today }

      it { is_expected.to be false }
    end

    context 'with a string' do
      let(:value) { 'X' }

      it { is_expected.to be false }
    end
  end

  describe '#call' do
    subject(:result) { attr.call(value) }

    context 'with a date' do
      let(:value) { Date.today }

      it 'throws an error' do
        expect { result }.to raise_error(RiceBubble::ValidationError)
      end
    end

    context 'with a time' do
      let(:value) { Time.now }

      it { is_expected.to eq value }
    end
  end
end
