require 'spec_helper'

RSpec.describe RiceBubble::Attributes::Literal do
  subject(:attr) { described_class.new(literal) }

  let(:literal) { 16 }

  describe '#valid?' do
    subject(:valid?) { attr.valid?(value) }

    context 'with the exact value' do
      let(:value) { 16 }

      it { is_expected.to be true }
    end

    context 'with a different value' do
      let(:value) { 69 }

      it { is_expected.to be false }
    end

    context 'with a different type' do
      let(:value) { '16' }

      it { is_expected.to be false }
    end
  end

  describe '#call' do
    subject(:result) { attr.call(value, path: 'level') }

    context 'with a literal' do
      let(:value) { 16 }

      it { is_expected.to eq 16 }
    end

    context 'with a string' do
      let(:value) { '16' }

      it 'throws an error' do
        expect { result }
          .to raise_error(
            RiceBubble::ValidationError,
            'level expected 16 but received "16"'
          )
      end
    end
  end
end
