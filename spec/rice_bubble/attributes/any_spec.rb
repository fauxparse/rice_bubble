require 'spec_helper'

RSpec.describe RiceBubble::Attributes::Any do
  subject(:attr) do
    described_class.new(
      RiceBubble::Attributes::Integer.new,
      RiceBubble::Attributes::String.new
    )
  end

  describe '#valid?' do
    subject(:valid?) { attr.valid?(value) }

    context 'with an integer' do
      let(:value) { 16 }

      it { is_expected.to be true }
    end

    context 'with a string' do
      let(:value) { '16' }

      it { is_expected.to be true }
    end

    context 'with an array' do
      let(:value) do
        [16, '16']
      end

      it { is_expected.to be false }
    end
  end

  describe '#call' do
    subject(:result) { attr.call(value, path: 'level') }

    context 'with an integer' do
      let(:value) { 16 }

      it { is_expected.to eq value }
    end

    context 'with a string' do
      let(:value) { '16' }

      it { is_expected.to eq value }
    end

    context 'with an array' do
      let(:value) do
        [16, '16']
      end

      it 'throws an error' do
        expect { result }.to raise_error(
          RiceBubble::ValidationError,
          'level expected one of [an integer, a string] ' \
          'but received [16, "16"]'
        )
      end
    end
  end
end
