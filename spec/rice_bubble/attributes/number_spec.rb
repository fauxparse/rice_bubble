require 'spec_helper'

RSpec.describe RiceBubble::Attributes::Number do
  subject(:attr) { described_class.new(**options) }

  let(:options) { {} }

  describe '#valid?' do
    subject(:valid?) { attr.valid?(value) }

    context 'with an integer' do
      let(:value) { 16 }

      it { is_expected.to be true }
    end

    context 'with a float' do
      let(:value) { Math::PI }

      it { is_expected.to be true }
    end

    context 'with a string' do
      let(:value) { '420' }

      it { is_expected.to be false }
    end

    context 'with a number that is too small' do
      let(:value) { 5 }
      let(:options) { { min: 10 } }

      it { is_expected.to be false }
    end

    context 'with a number that is too large' do
      let(:value) { 10 }
      let(:options) { { max: 5 } }

      it { is_expected.to be false }
    end
  end

  describe '#call' do
    subject(:result) { attr.call(value, path: 'number') }

    context 'with an integer' do
      let(:value) { 16 }

      it { is_expected.to eq 16 }
    end

    context 'with a float' do
      let(:value) { Math::PI }

      it { is_expected.to eq Math::PI }
    end

    context 'with a string' do
      let(:value) { '420' }

      it 'raises an error' do
        expect { result }.to raise_error(RiceBubble::ValidationError)
      end
    end
  end

  describe '#description' do
    subject(:description) { attr.description }

    context 'when there is a minimum but no maximum' do
      let(:attr) { described_class.new(min: 5) }

      it { is_expected.to eq 'a number greater than or equal to 5' }
    end

    context 'when there is a maximum but no minimum' do
      let(:attr) { described_class.new(max: 5) }

      it { is_expected.to eq 'a number less than or equal to 5' }
    end

    context 'when there is a maximum and a minimum' do
      let(:attr) { described_class.new(min: 1, max: 5) }

      it { is_expected.to eq 'a number between 1 and 5' }
    end
  end
end
