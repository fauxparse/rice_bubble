require 'spec_helper'

RSpec.describe RiceBubble::Attributes::Enum do
  subject(:attr) do
    described_class.new(
      'fighter',
      'wizard',
      'thief'
    )
  end

  describe '#valid?' do
    subject(:valid?) { attr.valid?(value) }

    context 'with a valid value' do
      let(:value) { 'wizard' }

      it { is_expected.to be true }
    end

    context 'with an invalid value' do
      let(:value) { 'bard' }

      it { is_expected.to be false }
    end
  end

  describe '#call' do
    subject(:result) { attr.call(value, path: 'class') }

    context 'with a valid class' do
      let(:value) { 'thief' }

      it { is_expected.to eq value }
    end

    context 'with an invalid class' do
      let(:value) { 'barbarian' }

      it 'throws an error' do
        expect { result }.to raise_error(
          RiceBubble::ValidationError,
          'class expected one of ["fighter", "wizard", "thief"] ' \
          'but received "barbarian"'
        )
      end
    end
  end
end
