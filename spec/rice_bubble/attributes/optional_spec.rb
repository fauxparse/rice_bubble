require 'spec_helper'

RSpec.describe RiceBubble::Attributes::Optional do
  subject(:attr) do
    described_class.new(RiceBubble::Attributes::String.new)
  end

  describe '#valid?' do
    subject(:valid?) { attr.valid?(value) }

    context 'with a string' do
      let(:value) { 'Spicy Beef' }

      it { is_expected.to be true }
    end

    context 'with nil' do
      let(:value) { nil }

      it { is_expected.to be true }
    end
  end

  describe '#call' do
    subject(:result) { attr.call(value) }

    context 'with a string' do
      let(:value) { 'Spicy Beef' }

      it { is_expected.to eq 'Spicy Beef' }
    end

    context 'with nil' do
      let(:value) { nil }

      it { is_expected.to be_nil }
    end
  end

  context 'with an optional integer' do
    subject(:attr) do
      RiceBubble::Attributes::Integer.new.optional
    end

    describe '#valid?' do
      subject(:valid?) { attr.valid?(value) }

      context 'with an integer' do
        let(:value) { 16 }

        it { is_expected.to be true }
      end

      context 'with a string' do
        let(:value) { 'Spicy Beef' }

        it { is_expected.to be false }
      end

      context 'with nil' do
        let(:value) { nil }

        it { is_expected.to be true }
      end
    end
  end

  describe '#optional' do
    subject(:attribute) { RiceBubble::Attributes::String.new.optional }

    it 'creates an optional attribute' do
      expect(attribute).to be_a described_class
    end

    it 'marks the attribute as optional' do
      expect(attribute.description).to eq 'a string (optional)'
    end
  end
end
