require 'spec_helper'

RSpec.describe RiceBubble::Attributes::Integer do
  subject(:attr) { described_class.new(**options) }

  let(:options) { {} }

  describe '#call' do
    subject(:result) { attr.call(value, path: 'level') }

    context 'with an integer' do
      let(:value) { 5 }

      it { is_expected.to eq 5 }
    end

    context 'with a number that is too small' do
      let(:value) { 5 }
      let(:options) { { min: 10 } }

      it 'raises an error' do
        expect { result }.to raise_error(
          RiceBubble::ValidationError,
          'level expected an integer greater than or equal to 10 but received 5'
        )
      end
    end

    context 'with a number that is too large' do
      let(:value) { 10 }
      let(:options) { { max: 5 } }

      it 'raises an error' do
        expect { result }.to raise_error(
          RiceBubble::ValidationError,
          'level expected an integer less than or equal to 5 but received 10'
        )
      end
    end

    context 'with nil' do
      let(:value) { nil }

      it 'raises an error' do
        expect { result }.to raise_error(
          RiceBubble::ValidationError,
          'level expected an integer but received nil'
        )
      end
    end
  end
end
