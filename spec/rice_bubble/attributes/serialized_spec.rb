require 'spec_helper'

RSpec.describe RiceBubble::Attributes::Serialized do
  subject(:attr) { described_class.new(character_class) }

  let(:character_class) do
    Class.new(RiceBubble::Serializer) do
      attributes(
        name: string,
        level: integer
      )

      def name
        object[:name].upcase
      end
    end
  end

  describe '#call' do
    subject(:result) { attr.call(value, path: 'class') }

    context 'with a valid value' do
      let(:value) do
        {
          name: 'wizard',
          level: 3
        }
      end

      let(:expected) do
        {
          name: 'WIZARD',
          level: 3
        }
      end

      it { is_expected.to eq expected }
    end

    context 'with an invalid value' do
      let(:value) do
        {
          name: 'wizard'
        }
      end

      it 'throws an error' do
        expect { result }
          .to raise_error(
            RiceBubble::ValidationError,
            'class.level expected an integer but received nil'
          )
      end
    end
  end
end
