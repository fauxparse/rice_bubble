require 'spec_helper'

RSpec.describe RiceBubble::Attributes::Boolean do
  subject(:attr) { described_class.new(**options) }

  let(:options) { {} }

  describe '#valid?' do
    subject(:valid?) { attr.valid?(value) }

    context 'with true' do
      let(:value) { true }

      it { is_expected.to be true }
    end

    context 'with false' do
      let(:value) { false }

      it { is_expected.to be true }
    end

    context 'with nil' do
      let(:value) { nil }

      it { is_expected.to be false }
    end

    context 'with a string' do
      let(:value) { '420' }

      it { is_expected.to be false }
    end
  end

  describe '#call' do
    subject(:result) { attr.call(value) }

    context 'with true' do
      let(:value) { true }

      it { is_expected.to be true }
    end

    context 'with false' do
      let(:value) { false }

      it { is_expected.to be false }
    end

    context 'with 0' do
      let(:value) { 0 }

      it 'throws an error' do
        expect { result }.to raise_error(RiceBubble::ValidationError)
      end
    end

    context 'with nil' do
      let(:value) { nil }

      it 'throws an error' do
        expect { result }.to raise_error(RiceBubble::ValidationError)
      end
    end
  end
end
