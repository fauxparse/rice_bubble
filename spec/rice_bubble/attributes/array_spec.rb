require 'spec_helper'

RSpec.describe RiceBubble::Attributes::Array do
  subject(:attr) { described_class.new(RiceBubble::Attributes::Integer.new) }

  describe '#valid?' do
    subject(:valid?) { attr.valid?(value) }

    context 'with a valid array' do
      let(:value) do
        [2, 3, 5, 7, 11, 13]
      end

      it { is_expected.to be true }
    end

    context 'with an invalid array' do
      let(:value) do
        [2, 3, 'buzz', 7, 11, 13]
      end

      it { is_expected.to be false }
    end
  end

  describe '#call' do
    subject(:result) { attr.call(value, path: 'primes') }

    context 'with a valid array' do
      let(:value) do
        [2, 3, 5, 7, 11, 13]
      end

      it { is_expected.to eq value }
    end

    context 'with an invalid array' do
      let(:value) do
        [2, 3, 'buzz', 7, 11, 13]
      end

      it 'throws an error' do
        expect { result }
          .to raise_error(
            RiceBubble::ValidationError,
            'primes[2] expected an integer but received "buzz"'
          )
      end
    end
  end

  context 'with an array of serializers' do
    subject(:attr) { described_class.new(character_class) }

    let(:character_class) do
      Class.new(RiceBubble::Serializer) do
        attributes(
          name: RiceBubble::Attributes::String.new,
          level: RiceBubble::Attributes::Integer.new
        )

        def level
          -object[:level]
        end
      end
    end

    describe '#call' do
      subject(:result) { attr.call(value, path: 'classes') }

      context 'with a valid array' do
        let(:value) do
          [
            {
              name: 'wizard',
              level: 3
            },
            {
              name: 'fighter',
              level: 1
            }
          ]
        end

        let(:expected) do
          [
            {
              name: 'wizard',
              level: -3
            },
            {
              name: 'fighter',
              level: -1
            }
          ]
        end

        it { is_expected.to eq expected }
      end
    end
  end
end
