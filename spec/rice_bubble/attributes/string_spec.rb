require 'spec_helper'

RSpec.describe RiceBubble::Attributes::String do
  subject(:attr) { described_class.new(**options) }

  let(:options) { {} }

  describe '#call' do
    subject(:result) { attr.call(value, path: 'name') }

    context 'with a string' do
      let(:value) { 'Spicy Beef' }

      it { is_expected.to eq 'Spicy Beef' }
    end

    context 'with a number' do
      let(:value) { 1 }

      it 'raises an error' do
        expect { result }.to raise_error(
          RiceBubble::ValidationError,
          'name expected a string but received 1'
        )
      end
    end

    context 'with a symbol' do
      let(:value) { :spicy_beef }

      it { is_expected.to eq 'spicy_beef' }
    end

    context 'with a string that is too short' do
      let(:value) { 'X' }
      let(:options) { { min: 2 } }

      it 'raises an error' do
        expect { result }.to raise_error(
          RiceBubble::ValidationError,
          'name expected a string of at least 2 characters but received "X"'
        )
      end
    end

    context 'with a string that is too long' do
      let(:value) { 'Spicy Beef' }
      let(:options) { { max: 5 } }

      it 'raises an error' do
        expect { result }.to raise_error(
          RiceBubble::ValidationError,
          'name expected a string of up to 5 characters but received "Spicy Beef"'
        )
      end
    end

    context 'with a string that does not match the format' do
      let(:value) { 'Spicy Beef' }
      let(:options) { { format: /\A[a-z]+\z/ } }

      it 'raises an error' do
        expect { result }.to raise_error(
          RiceBubble::ValidationError,
          'name expected a string matching /\\A[a-z]+\\z/ but received "Spicy Beef"'
        )
      end
    end
  end

  describe '#description' do
    subject(:description) { attr.description }

    context 'when there is a minimum but no maximum' do
      let(:attr) { described_class.new(min: 5) }

      it { is_expected.to eq 'a string of at least 5 characters' }
    end

    context 'when there is a maximum but no minimum' do
      let(:attr) { described_class.new(max: 5) }

      it { is_expected.to eq 'a string of up to 5 characters' }
    end

    context 'when there is a maximum and a minimum' do
      let(:attr) { described_class.new(min: 1, max: 5) }

      it { is_expected.to eq 'a string between 1 and 5 characters long' }
    end

    context 'when there is a format' do
      let(:attr) { described_class.new(format: /^[a-z]$/) }

      it { is_expected.to eq 'a string matching /^[a-z]$/' }
    end
  end
end
