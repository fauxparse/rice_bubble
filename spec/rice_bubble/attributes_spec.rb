require 'spec_helper'

RSpec.describe RiceBubble::Attributes do
  subject(:attributes) { described_class.new }

  describe '#each' do
    subject(:result) { attributes.each }

    it { is_expected.to be_a Enumerator }
  end

  describe '#[]' do
    subject(:result) { attributes[:name] }

    let(:attributes) do
      described_class.new(name: RiceBubble::Attributes::String.new)
    end

    it { is_expected.to be_a RiceBubble::Attributes::String }
  end

  describe '.[]' do
    it 'returns a valid type' do
      expect(described_class[:string]).to eq RiceBubble::Attributes::String
    end

    it 'returns nil for invalid types' do
      expect(described_class[:invalid]).to be_nil
    end
  end
end
