require_relative 'rice_bubble/version'
require_relative 'rice_bubble/attributes'
require_relative 'rice_bubble/attributes/base'
require_relative 'rice_bubble/attributes/any'
require_relative 'rice_bubble/attributes/array'
require_relative 'rice_bubble/attributes/boolean'
require_relative 'rice_bubble/attributes/date'
require_relative 'rice_bubble/attributes/datetime'
require_relative 'rice_bubble/attributes/enum'
require_relative 'rice_bubble/attributes/number'
require_relative 'rice_bubble/attributes/integer'
require_relative 'rice_bubble/attributes/literal'
require_relative 'rice_bubble/attributes/object'
require_relative 'rice_bubble/attributes/optional'
require_relative 'rice_bubble/attributes/serialized'
require_relative 'rice_bubble/attributes/string'
require_relative 'rice_bubble/attributes/time'
require_relative 'rice_bubble/serializer'

module RiceBubble
  class ValidationError < StandardError
    attr_reader :value, :path, :attribute

    def initialize(value:, path:, attribute:)
      super("#{path} expected #{attribute.description} but received #{value.inspect}")
    end
  end
end
