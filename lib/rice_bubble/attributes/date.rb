module RiceBubble
  class Attributes
    class Date < Base
      def call(value, path: '')
        super(value.to_date, path:)
      end

      def valid_types
        [::Date, ::Time, ::DateTime]
      end
    end
  end
end
