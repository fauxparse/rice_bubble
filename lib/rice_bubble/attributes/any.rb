module RiceBubble
  class Attributes
    class Any < Base
      attr_reader :members

      def initialize(*members, &)
        super(&)
        @members = members.flatten
      end

      def valid?(value)
        !which(value).nil?
      end

      def call(value, path: '')
        super(which(value)&.call(value, path:) || value, path:)
      end

      def which(value)
        members.find { |member| member.valid?(value) }
      end

      def description
        "one of [#{members.map(&:description).join(", ")}]"
      end
    end
  end
end
