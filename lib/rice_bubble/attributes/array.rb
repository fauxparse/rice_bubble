module RiceBubble
  class Attributes
    class Array < Base
      attr_reader :members

      def initialize(members, &)
        super(&)
        @members = instantiate(members)
      end

      def valid?(value)
        return false unless value.respond_to?(:all?)

        value.all? do |child|
          members.valid?(child)
        end
      end

      def call(value, path: '')
        (value || []).map.with_index do |child, index|
          members.call(child, path: "#{path}[#{index}]")
        end
      end
    end
  end
end
