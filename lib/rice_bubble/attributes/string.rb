module RiceBubble
  class Attributes
    class String < Base
      attr_reader :min, :max, :format

      def initialize(min: nil, max: nil, format: nil, &)
        super(&)
        @min = min
        @max = max
        @format = format
      end

      def valid_types
        [::String, Symbol]
      end

      def valid?(value)
        super &&
          !min&.send(:>, value.to_s.length) &&
          !max&.send(:<, value.to_s.length) &&
          (!format || format.match?(value.to_s))
      end

      def call(value, path: '')
        super.to_s
      end

      def description
        result = super

        if min && max
          result = "#{result} between #{min} and #{max} characters long"
        elsif min
          result = "#{result} of at least #{min} characters"
        elsif max
          result = "#{result} of up to #{max} characters"
        end

        result = "#{result} matching #{format.inspect}" if format

        result
      end
    end
  end
end
