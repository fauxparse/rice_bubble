module RiceBubble
  class Attributes
    class Serialized < Base
      attr_reader :serializer

      def initialize(serializer, &)
        super(&)
        @serializer = serializer
      end

      def valid?(value)
        !value.nil?
      end

      def call(value, path: '')
        serializer.call(value, path:)
      end
    end
  end
end
