module RiceBubble
  class Attributes
    class Number < Base
      attr_reader :min, :max

      def initialize(min: nil, max: nil, &)
        super(&)
        @min = min
        @max = max
      end

      def valid_types
        [::Numeric]
      end

      def valid?(value)
        super &&
          !min&.send(:>, value) &&
          !max&.send(:<, value)
      end

      def description
        result = super

        if min && max
          "#{result} between #{min} and #{max}"
        elsif min
          "#{result} greater than or equal to #{min}"
        elsif max
          "#{result} less than or equal to #{max}"
        else
          result
        end
      end
    end
  end
end
