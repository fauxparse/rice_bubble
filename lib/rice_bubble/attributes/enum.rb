module RiceBubble
  class Attributes
    class Enum < Any
      def initialize(*members, &)
        super(*members.flatten.map { |m| Literal.new(m) }, &)
      end
    end
  end
end
