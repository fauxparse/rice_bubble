module RiceBubble
  class Attributes
    class Optional < Base
      attr_reader :child

      def initialize(child, &)
        super(&)
        @child = child
      end

      def valid?(value)
        value.nil? || child.valid?(value)
      end

      def call(value, path: '')
        value && child.call(value, path:)
      end

      def description
        "#{child.description} (optional)"
      end
    end
  end
end
