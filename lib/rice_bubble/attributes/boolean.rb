module RiceBubble
  class Attributes
    class Boolean < Base
      def valid?(value)
        [true, false].include?(value)
      end
    end
  end
end
