module RiceBubble
  class Attributes
    class Integer < Number
      attr_reader :min, :max

      def initialize(min: nil, max: nil, **options, &)
        super(**options, &)
        @min = min
        @max = max
      end

      def valid_types
        [::Integer]
      end

      def valid?(value)
        super &&
          (!min || value >= min) &&
          (!max || value <= max)
      end
    end
  end
end
