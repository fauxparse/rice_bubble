module RiceBubble
  class Attributes
    class Time < Base
      def valid?(value)
        return false unless valid_time?(value)

        value.respond_to?(:to_time)
      end

      def coerce(value)
        return nil unless valid_time?(value)

        value
      end

      private

      def valid_time?(value)
        case value
        when ::DateTime then true
        when ::Date then false
        else value.respond_to?(:to_time)
        end
      end
    end
  end
end
