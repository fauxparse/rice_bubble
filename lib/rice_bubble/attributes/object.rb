module RiceBubble
  class Attributes
    class Object < Base
      attr_reader :children

      def initialize(children = {}, &)
        super(&)
        @children = Attributes.new(
          children.transform_values(&method(:instantiate))
        )
      end

      def valid?(value)
        children.all? do |name, attr|
          attr.valid?(value[name])
        end
      end

      def call(value, path: '')
        children.to_h do |name, attr|
          [name,
           attr.call(value[name] || value[name.to_s], path: "#{path}.#{name}")]
        end
      end
    end
  end
end
