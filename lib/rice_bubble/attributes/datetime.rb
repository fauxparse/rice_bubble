module RiceBubble
  class Attributes
    class Datetime < Base
      def call(value, path: '')
        super(value, path:)
      end

      def valid_types
        [::Time, ::DateTime]
      end
    end
  end
end
