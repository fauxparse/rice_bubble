module RiceBubble
  class Attributes
    class Literal < Base
      attr_reader :literal

      def initialize(literal, &)
        super(&)
        @literal = literal
      end

      def valid?(value)
        value == literal
      end

      def description
        literal.inspect
      end
    end
  end
end
