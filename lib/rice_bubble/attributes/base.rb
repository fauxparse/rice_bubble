module RiceBubble
  class Attributes
    class Base
      def initialize(&block)
        @fetcher = block
      end

      def fetch(object, name)
        if @fetcher
          @fetcher.call(object, name)
        elsif object.respond_to?(name)
          object.public_send(name)
        else
          object[name] || object[name.to_s]
        end
      end

      def valid?(value)
        valid_types.any? { |t| value.is_a?(t) }
      end

      def valid_types
        [::Object]
      end

      def call(value, path: '')
        if valid?(value)
          value
        else
          validation_error(value:, path:)
        end
      end

      def optional
        Optional.new(self)
      end

      def description
        soft_name = self.class.name.split('::').last
          .gsub(/([a-z])([A-Z])/, '\1 \2')
          .downcase
        article = soft_name.start_with?(/[aeiou]/) ? 'an' : 'a'
        "#{article} #{soft_name}"
      end

      private

      def instantiate(class_or_instance)
        if class_or_instance.is_a?(Class) && class_or_instance < Serializer
          class_or_instance.new
        else
          class_or_instance
        end
      end

      def validation_error(value:, path: '')
        raise ValidationError.new(value:, path:, attribute: self)
      end
    end
  end
end
