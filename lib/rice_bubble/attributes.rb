module RiceBubble
  class Attributes
    attr_reader :attributes

    include Enumerable

    def initialize(attrs = {})
      @attributes = {}
      attrs.each do |name, attr|
        self[name] = attr
      end
    end

    def initialize_copy(other)
      super
      @attributes = other.attributes.dup
    end

    def each(&)
      if block_given?
        attributes.each(&)
      else
        to_enum(:each)
      end
    end

    def include?(name)
      attributes.key?(name.to_sym)
    end

    def [](name)
      attributes[name.to_sym]
    end

    def []=(name, attr)
      attributes[name] = attr
    end

    def map(&)
      attributes.keys.each.with_object({}) do |name, hash|
        hash[name] = yield(name, attributes[name])
      end
    end

    def self.[](type)
      @types ||= {}
      @types[type] ||=
        begin
          class_name = type.to_s.gsub(/(^|_)(\w)/) do
            ::Regexp.last_match(2).upcase
          end
          Object.const_get "RiceBubble::Attributes::#{class_name}"
        end
    rescue NameError
      nil
    end
  end
end
